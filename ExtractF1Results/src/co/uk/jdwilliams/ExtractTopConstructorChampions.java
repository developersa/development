package co.uk.jdwilliams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class ExtractTopConstructorChampions {

	public ExtractTopConstructorChampions() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<String> getTopConstructorTeams(URL url, int limit) throws IOException {
		ArrayList<String> constructorTeams = new ArrayList<String>(limit);
		URLConnection con = url.openConnection();
		InputStream is = con.getInputStream();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line = null;

			int counter = 1;
			while ((line = br.readLine()) != null) {
				int start = line.indexOf("<table class=\"motor-sport-results msr_season_team_results\">");
				int end = line.indexOf("</table>");

				if (start != -1) {

					String table = line.substring(start, end);

					int end1 = table.indexOf("<tbody>");

					table = table.replaceAll(table.substring(0, end1), "<td>");

					String[] splitRows = table.split("<tr>");

					for (int i = 1; i < 11; i = i + 2) {

						int nameSIndex = splitRows[i].indexOf(" title='") + 8;
						int pointSIndex = splitRows[i].indexOf("class=\"msr_total\">") + 18;
						String constructorName = splitRows[i].substring(nameSIndex, nameSIndex + 15);
						int indexConstName = constructorName.indexOf("'>");
						constructorName = constructorName.substring(0, indexConstName);

						constructorTeams.add("Number # " + counter + " = " + constructorName + " Total Points = "
								+ splitRows[i].substring(pointSIndex, pointSIndex + 3));
						counter++;

					}

				}
				// return constructorTeams;

			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new IOException();
		}
		return constructorTeams;
	}

}
