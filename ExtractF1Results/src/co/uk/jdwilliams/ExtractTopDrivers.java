package co.uk.jdwilliams;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ExtractTopDrivers {

	public ExtractTopDrivers() {

	}

	public Elements getRows(URL url) throws IOException {
		Document doc = Jsoup.parse(url, 40000);

		// Element table =
		// doc.select("table").get(1).select("table.motor-sport-results");
		// TODO Need to automatically find correct table by using class names

		Element table = doc.select("table").get(1); // getting table with
													// drivers result
		Elements rows = table.select("tr");

		return rows;
	}

	public ArrayList<String> getTopDrivers(Elements rows, int limit) {
		ArrayList<String> list = new ArrayList<String>(limit);
		int counter = 1;

		for (int i = 1; i < rows.size(); i++) {

			Element row = rows.get(i); // rows
			Elements cols = row.select("td"); // colums

			HashMap<Elements, Elements> map = new HashMap<Elements, Elements>();
			Elements key = cols.select("td.msr_driver").select("a[title]");
			key.subList(1, 1).stream();
			// removeAttr("<a
			// href=");key.removeAttr("/");key.removeAttr("</a>");
			// map.put(cols.select("td.msr_driver").select("a[title]"),
			// cols.select("td.msr_total"));
			map.put(key, cols.select("td.msr_total"));
			// sSystem.out.println(map);

			// String [] drivers = new String [10];
			String driverName = key.toString();
			int indexKey = driverName.indexOf(" title=\"") + 8;
			driverName = driverName.substring(indexKey, indexKey + 20);
			int indexKey1 = driverName.indexOf(">");
			driverName = driverName.substring(0, indexKey1 - 1);
			// System.out.println(driverName);

			String driverPoints = cols.select("td.msr_total").toString();
			int pointSIndex = driverPoints.indexOf("class=\"msr_total\">") + 18;
			driverPoints = driverPoints.substring(pointSIndex, pointSIndex + 3);
			int pointEIndex = driverPoints.indexOf("<");
			if (pointEIndex > 0)
				driverPoints = driverPoints.substring(0, pointEIndex);
			// System.out.println("No#"+counter + " Driver Name = " + driverName
			// + " --> points = " + driverPoints);
			list.add("No#" + counter + " Driver = " + driverName + " : points = " + driverPoints);
			counter++;
			if (counter == limit) {
				break;
			}
		}

		return list;
	}

}
