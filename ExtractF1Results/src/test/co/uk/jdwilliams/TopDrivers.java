package test.co.uk.jdwilliams;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.select.Elements;
import org.junit.Test;

import co.uk.jdwilliams.ExtractTopDrivers;
import utility.co.uk.jdwilliams.GlobalConstants;

public class TopDrivers {

	@Test
	public void test() throws IOException {
		ExtractTopDrivers extractTopDrivers = new ExtractTopDrivers();
		URL url = new URL(GlobalConstants.URL);
		Elements rows = extractTopDrivers.getRows(url );
		ArrayList<String> topDrivers = extractTopDrivers.getTopDrivers(rows, GlobalConstants.DRIVERLIMIT);
		assertEquals(10, topDrivers.size());
	}

}
