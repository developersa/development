package utility.co.uk.jdwilliams;

public final class GlobalConstants {
	
	public GlobalConstants(){
		
	}
	public final static int DRIVERLIMIT = 11;
	public final static int TEAMSLIMIT = 5;
	public final static String URL = "http://www.f1-fansite.com/f1-results/2015-f1-results-standings/";
}
