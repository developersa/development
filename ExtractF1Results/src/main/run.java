package main;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.codehaus.jettison.json.JSONArray;
import org.jsoup.select.Elements;

import co.uk.jdwilliams.ExtractTopConstructorChampions;
import co.uk.jdwilliams.ExtractTopDrivers;
import utility.co.uk.jdwilliams.GlobalConstants;

public class run {

	public static void main(String[] args) throws IOException {

		URL url = new URL(GlobalConstants.URL);
		ExtractTopDrivers topDrivers = new ExtractTopDrivers();
		Elements rows = topDrivers.getRows(url);
		ArrayList<String> driverList = topDrivers.getTopDrivers(rows, GlobalConstants.DRIVERLIMIT);
		JSONArray driversJSON = new JSONArray(driverList);
		System.out.println(driversJSON);
		
		ExtractTopConstructorChampions topTeam = new ExtractTopConstructorChampions();
		ArrayList<String> topTeams = topTeam.getTopConstructorTeams(url, GlobalConstants.TEAMSLIMIT);
		JSONArray teamsJSON = new JSONArray(topTeams);
		System.out.println(teamsJSON);
	}

}
